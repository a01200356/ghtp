import java.io.IOException;
import java.io.PushbackReader;
import java.io.StringReader;
import java.util.Scanner;

import evaluator.Evaluator;

import ghtp.lexer.*;
import ghtp.parser.*;
import ghtp.node.*;

public class Main {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) {
		if (args.length < 1) {
			System.err.println("Query not found.");
			System.exit(-1);
		}
		String line = args[0];
		
		// Try to parse and run line.
		try {
			Lexer lex = new Lexer(new PushbackReader(new StringReader(line)));
			Parser p = new Parser(lex);
			Start tree = p.parse();
			Evaluator.eval(tree);
			
		} catch (ParserException | LexerException | IOException | RuntimeException e) {
			System.err.println("There was an error while running your query:");
			System.err.println(e.getMessage());
			//e.printStackTrace();
		}
	}
}