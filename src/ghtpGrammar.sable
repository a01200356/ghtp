Package ghtp;

Helpers
  digit      = ['0'..'9'] ;
  anything   = [0x000020..0x02ffff] ;
  character  = '\' anything | '%' | ['A'..'Z'] | ['a'..'z'] | [0x0000a1..0x02ffff] ;

Tokens
  space  = ' ' | 9 | 10 | 13 ;
  qmark  = '?' ;
  op_par = '(' ;
  cl_par = ')' ;
  comma  = ',' | ';' ;
  op_br  = '{' ;
  cl_br  = '}' ;
  /*
   * Reserved words
   */
  get     = 'get ' | 'GET ' ;
  post    = 'post ' | 'POST ' ;
  put     = 'put ' | 'PUT ' ;
  delete  = 'delete ' | 'DELETE ' ;
  /*
   * Operations
   */
  and     = '&' ;
  or      = '|' ;
  equals  = '=' ;
  like    = '~=' ;
  unequal = '!=' ;
  unlike  = '!~=' | '!~' ;
  gt      = '>' ;
  lt      = '<' ;
  ge      = '>=' ;
  le      = '<=' ;
  plus    = '+' ;
  minus   = '-' ;
  prod    = '*' ;
  div     = '/' ;
  append  = '+=' ;
  /*
   * Types
   */
  number  = digit* ( '.' digit+ )? ( ('e'|'E') '-'?  digit+ )? ;
  tag     = character (character | digit)* ;
  string  = '"' ([anything-'"'] | '\"' )* '"' ;
  path    = '.'* '/' (character|'/'|'.')+ ;

Ignored Tokens
  space ;

Productions
  request =
    {get}    get    path cols_selection?    qmark? query? |
    {delete} delete path cols_selection?    qmark  query  |
    {post}   post   path cols_assignation   qmark?        |
    {put}    put    path cols_modification  qmark?  query? ;

  cols_assignation =
    op_br assignation extra_assignation* cl_br ;

  assignation =
    tag equals value ;

  extra_assignation =
    comma assignation ;

  cols_modification =
    op_br modification extra_modification* cl_br ;

  modification =
    {assign} tag equals value |
    {append} tag append value ;

  extra_modification =
    comma modification ;

  cols_selection =
    op_br column extra_column* cl_br ;

  column =
    {tag} tag ;

  extra_column =
    comma column ;

  query =
    {single}      condition                         |
    {multiple}    condition logical_operation query ;

  condition =
    {comparison}          tag equality value    |
    {par}                 op_par query cl_par ;

  extra_parameter =
    comma value ;

  value =
    {string}  string                   |
    {number}  number                   |
    {neg_num} minus number             |
    {tag}     tag                      |
    {str_op}  string operation value   |
    {num_op}  number operation value   |
    {tag_op}  tag operation value      |
    {par}     op_par value cl_par      |
    {par_op}  op_par [inside_value]:value cl_par operation [outside_value]:value ;

  operation =
    {equals} equals | {plus} plus | {minus} minus | {prod} prod | {div} div | {append} append ;

  equality =
    {equals} equals | {like} like | {unequal} unequal | {unlike} unlike |
    {lt} lt | {gt} gt | {le} le | {ge} ge ;

  logical_operation =
    {and} and | {or} or ;
