package evaluator;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import ghtp.analysis.*;
import ghtp.node.*;

import com.opencsv.*;

public class Evaluator extends DepthFirstAdapter {

	// Static call to evaluate entire tree and print results.
	public static void eval(Node treeRoot) {
		Evaluator e = new Evaluator();
		treeRoot.apply(e);
	}
	
	// Structure to hold calculated values.
	private HashMap<Node, Object> values = new HashMap<Node, Object>();
	
	// Structure to handle CSV file.
	private List<String[]> csvRows;
	
	// key is index, value is the value to be assigned to the column
	private HashMap<Integer, Object> selectedColumns;
	
	// Tries to read CSV file from the path.
	private void readCSVFile(String path) {
		path = path.trim();
		try {
			CSVReader csvReader = new CSVReader(new FileReader(path));
			csvRows = csvReader.readAll();
			csvReader.close();
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	
	// Tries to write CSV file from the path.
	private void writeCSVFile(String path, List<String[]> rows) {
		path = path.trim();
		try {
			CSVWriter csvWriter = new CSVWriter(new BufferedWriter(new FileWriter(path)));
			csvWriter.writeAll(rows);
			csvWriter.close();
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	
	// Search column from query in file's header.
	private int getColumnIndex(String colName) {
		int colIndex = -1;
		String[] columns = csvRows.get(0);
		for (int i = 0; i < columns.length; i++) {
			if (colName.equalsIgnoreCase(columns[i])) {
				colIndex = i;
				break;
			}
		}
		
		// If selected column was not found in file.
		if (colIndex == -1)
			throw new RuntimeException("Column " + colName + " not found.");
		
		return colIndex;
	}
	
	@Override
	public void caseAGetRequest(AGetRequest node) {
		readCSVFile(node.getPath().toString());
		super.caseAGetRequest(node);
	}

	@Override
	public void caseADeleteRequest(ADeleteRequest node) {
		readCSVFile(node.getPath().toString());
		super.caseADeleteRequest(node);
	}

	@Override
	public void caseAPostRequest(APostRequest node) {
		readCSVFile(node.getPath().toString());
		super.caseAPostRequest(node);
	}

	@Override
	public void caseAPutRequest(APutRequest node) {
		readCSVFile(node.getPath().toString());
		super.caseAPutRequest(node);
	}
	
	@Override
	public void inAColsSelection(AColsSelection node) {
		selectedColumns = new HashMap<Integer, Object>();
	}
	
	@Override
	public void inAColsAssignation(AColsAssignation node) {
		selectedColumns = new HashMap<Integer, Object>();
	}
	
	@Override
	public void inAColsModification(AColsModification node) {
		selectedColumns = new HashMap<Integer, Object>();
	}
	
	@Override
	public void outATagColumn(ATagColumn node) {
		int colIndex = getColumnIndex(node.getTag().toString().trim());
		selectedColumns.put(colIndex, null);
	}
	
	@Override
	public void outAAssignation(AAssignation node) {
		int colIndex = getColumnIndex(node.getTag().toString().trim());
		selectedColumns.put(colIndex, values.get(node.getValue()));
	}
	
	@Override
	public void outAAssignModification(AAssignModification node) {
		int colIndex = getColumnIndex(node.getTag().toString().trim());
		selectedColumns.put(colIndex, values.get(node.getValue()));
	}
	
	@Override
	public void outAAppendModification(AAppendModification node) {
		int colIndex = getColumnIndex(node.getTag().toString().trim());
		
		ArrayList<Object> rowValues = new ArrayList<Object>();
		
		Object value = values.remove(node.getValue());
		for (String[] row : csvRows.subList(1, csvRows.size())) {
			String colValue = row[colIndex].trim();
			
			Object result;
			if (value instanceof Double) {
				result = numOperation(strToNum(colValue), strToNum(value.toString()), new AAppendOperation());
			} else {
				result = strOperation(colValue, value.toString(), new AAppendOperation());
			}
			
			rowValues.add(result);
		}
		
		selectedColumns.put(colIndex, rowValues);
	}
	
	@Override
	public void outASingleQuery(ASingleQuery node) {
		values.put(node, values.remove(node.getCondition()));
	}

	@Override
	public void outAMultipleQuery(AMultipleQuery node) {
		ArrayList<Integer> filterA = (ArrayList<Integer>)values.remove(node.getCondition());
		ArrayList<Integer> filterB = (ArrayList<Integer>)values.remove(node.getQuery());
		switch (node.getLogicalOperation().getClass().getSimpleName()) {
			case "AAndLogicalOperation":
				filterA.retainAll(filterB);
				values.put(node, filterA);
				break;
			case "AOrLogicalOperation":
				filterA.removeAll(filterB);
				filterA.addAll(filterB);
				values.put(node, filterA);
				break;
		}
	}
	
	@Override
	public void outAParCondition(AParCondition node) {
		values.put(node, values.remove(node.getQuery()));
	}
	
	// Try to get number out of string.
	private Double strToNum(String str) {
		double value;
		// Null is read as a zero.
		if (str.isEmpty() || str.equalsIgnoreCase("null")) {
			value = 0;
			return value;
		}
		
		try {
			value = Double.valueOf(str);
		} catch (NumberFormatException e1) {
			try {
				value = Double.valueOf(str.substring(1));
			} catch (NumberFormatException e2) {
				value = Double.valueOf(str.substring(0, str.length()-1));
			}
		}
		return value;
	}
	
	@Override
	public void outAComparisonCondition(AComparisonCondition node) {
		ArrayList<Integer> conditionRows = new ArrayList<Integer>();
		
		String colName = node.getTag().toString().trim();
		int colIndex = getColumnIndex(colName);
		
		// Get value and check if it's list or scalar.
		Object value = values.remove(node.getValue());
		Object[] rowValues = null;
		if (value instanceof ArrayList) {
			rowValues = ((ArrayList<Object>)value).toArray();
		}
		
		// Run condition on each row of the file and add complying rows to list.
		int i = 1;
		for (String[] row : csvRows.subList(1, csvRows.size())) {
			if (rowValues != null) {
				value = rowValues[i-1];
			}
			
			boolean match = false;
			boolean opposite = false;
			switch (node.getEquality().getClass().getSimpleName()) {
				case "AUnequalEquality":
					opposite = true;
				case "AEqualsEquality":
					if (value instanceof Double) {
						if (strToNum(row[colIndex]).equals(value))
							match = true;
						break;
					}
					if (row[colIndex].equals(value))
						match = true;
					break;
					
				case "AUnlikeEquality":
					opposite = true;
				case "ALikeEquality":
					// In double, alike means that the difference is in a lower order of magnitude.
					if (value instanceof Double) {
						double number = (Double)value;
						double difference = strToNum(row[colIndex]) - number;
						
						// If they are the same.
						if (difference == 0) {
							match = true;
						} else {
							// Get orders of magnitude.
							int resultMagnitude = (int)Math.log10(Math.abs(difference));
							int paramsMagnitude;
							double minParam = Math.min(number, strToNum(row[colIndex]));
							if (minParam == 0)
								paramsMagnitude = 0;
							else
								paramsMagnitude = (int)Math.log10(Math.abs(minParam));
							
							// If the difference is less than an order of magnitude.
							if (resultMagnitude < paramsMagnitude)
								match = true;
						}
					} else {
						// In string, alike means that the first is a substring of the second.
						if (row[colIndex].toString().trim().contains(value.toString()))
							match = true;
					}
					break;
				case "ALeEquality":
					opposite = true;
				case "AGtEquality":
					if (value instanceof Double) {
						if (strToNum(row[colIndex]) > (double)value)
							match = true;
						break;
					}
					if (row[colIndex].compareToIgnoreCase((value.toString())) > 0)
						match = true;
					break;
				case "AGeEquality":
					opposite = true;
				case "ALtEquality":
					if (value instanceof Double) {
						if (strToNum(row[colIndex]) < (double)value)
							match = true;
						break;
					}
					if (row[colIndex].compareToIgnoreCase((value.toString())) < 0)
						match = true;
					break;
			}
			
			// If row matches condition xor if the condition was an opposite.
			if (match ^ opposite) {
				conditionRows.add(i);
			}
			i++;
		}

		values.put(node, conditionRows);
	}

	@Override
	public void outAStringValue(AStringValue node) {
		values.put(node, node.toString().trim().substring(1, node.toString().trim().length()-1));
	}

	@Override
	public void outANumberValue(ANumberValue node) {
		values.put(node, strToNum(node.getNumber().toString()));
	}
	
	@Override
	public void outANegNumValue(ANegNumValue node) {
		values.put(node, -1*strToNum(node.getNumber().toString()));
	}

	@Override
	public void outATagValue(ATagValue node) {
		ArrayList<Object> rowValues = new ArrayList<Object>();
		
		String colName = node.getTag().toString().trim();
		int colIndex = getColumnIndex(colName);
		
		for (String[] row : csvRows.subList(1, csvRows.size())) {
			String value = row[colIndex];
			
			// Dirty hack to check if it can be parsed as a number.
			try {
				double numVal = strToNum(value);
				rowValues.add(numVal);
			} catch(Exception e) {
				rowValues.add(value);
			}
		}
		
		values.put(node, rowValues);
	}

	private String strOperation(String a, String b, Node operation) {
		switch (operation.getClass().getSimpleName()) {
			case "APlusOperation":
			case "AAppendOperation":
				return a + b;
			case "AMinusOperation":
				return a.replace(b, "");
			default:
				throw new RuntimeException("Invalid operation between strings.");
		}
	}
	
	private double numOperation(Double a, Double b, Node operation) {
		switch (operation.getClass().getSimpleName()) {
			case "APlusOperation":
			case "AAppendOperation":
				return a + b;
			case "AMinusOperation":
				return a - b;
			case "AProdOperation":
				return a * b;
			case "ADivOperation":
				return a / b;
			default:
				throw new RuntimeException("Invalid operation between numbers.");
		}
	}
	
	@Override
	public void outAStrOpValue(AStrOpValue node) {
		String a, b;
		a = node.getString().toString().trim();
		a = a.substring(1, a.length()-1);
		
		Object value = values.remove(node.getValue());
		if (value instanceof ArrayList) {
			ArrayList<Object> results = new ArrayList<Object>();
			ArrayList<Object> rowValues = (ArrayList<Object>)value;
			for (Object row : rowValues) {
				b = row.toString();
				results.add(strOperation(a, b, node.getOperation()));
			}
			values.put(node, results);
		} else {
			b = value.toString();
			values.put(node, strOperation(a, b, node.getOperation()));
		}
	}

	@Override
	public void outANumOpValue(ANumOpValue node) {
		double a, b;
		a = strToNum(node.getNumber().toString());
		
		Object value = values.remove(node.getValue());
		if (value instanceof ArrayList) {
			ArrayList<Object> results = new ArrayList<Object>();
			ArrayList<Object> rowValues = (ArrayList<Object>)value;
			for (Object row : rowValues) {
				b = strToNum(row.toString());
				results.add(numOperation(a, b, node.getOperation()));
			}
			values.put(node, results);
		} else {
			b = strToNum(value.toString());
			values.put(node, numOperation(a, b, node.getOperation()));
		}
	}

	@Override
	public void outATagOpValue(ATagOpValue node) {
		ArrayList<Object> results = new ArrayList<Object>();
		
		// Get value and check if it's list or scalar.
		Object value = values.remove(node.getValue());
		Object[] rowValues = null;
		if (value instanceof ArrayList) {
			rowValues = ((ArrayList<Object>)value).toArray();
		}
		
		// Dirty hack to force it to actually parse tag.
		ATagValue tag = new ATagValue();
		tag.setTag(node.getTag());
		outATagValue(tag);
		
		int i = 1;
		for (Object row : (ArrayList<Object>)values.remove(tag)) {
			if (rowValues != null) {
				value = rowValues[i-1];
			}
			Object result;
			if (value instanceof Double) {
				result = numOperation(strToNum(row.toString()), strToNum(value.toString()), node.getOperation());
			} else {
				result = strOperation(row.toString(), value.toString(), node.getOperation());
			}
			
			results.add(result);
			i++;
		}

		values.put(node, results);
	}

	@Override
	public void outAParOpValue(AParOpValue node) {
		Object a = values.remove(node.getInsideValue());
		Object b = values.remove(node.getOutsideValue());
		
		Object[] aValues = null;		
		if (a instanceof ArrayList) {
			aValues = ((ArrayList<Object>)a).toArray();
		}
		Object[] bValues = null;
		if (b instanceof ArrayList) {
			bValues = ((ArrayList<Object>)b).toArray();
		}
		
		if (aValues != null || bValues != null) {
			ArrayList<Object> results = new ArrayList<Object>();
			for (int i = 0; i < csvRows.size()-1; i++) {
				if (aValues != null) {
					a = aValues[i];
				}
				if (bValues != null) {
					b = bValues[i];
				}
				
				if (a instanceof Double) {
					double numA = strToNum(a.toString());
					double numB = strToNum(b.toString());
					results.add(numOperation(numA, numB, node.getOperation()));
				} else {
					String strA = a.toString();
					String strB = b.toString();
					results.add(strOperation(strA, strB, node.getOperation()));
				}
			}
			values.put(node, results);
		} else {
			if (a instanceof Double) {
				double numA = strToNum(a.toString());
				double numB = strToNum(b.toString());
				values.put(node, numOperation(numA, numB, node.getOperation()));
			} else {
				String strA = a.toString();
				String strB = b.toString();
				values.put(node, strOperation(strA, strB, node.getOperation()));
			}
		}
	}
	
	@Override
	public void outAParValue(AParValue node) {
		values.put(node, values.remove(node.getValue()));
	}
	
	@Override
	public void outAGetRequest(AGetRequest node) {
		printTable((ArrayList<Integer>)values.remove(node.getQuery()));
	}
	
	@Override
	// Add new row.
	public void outAPostRequest(APostRequest node) {
		int nColumns = csvRows.get(0).length;
		// Create row with empty columns.
		String[] row = new String[nColumns];
		Arrays.fill(row, "");
		
		assignRow(row);
		
		csvRows.add(row);
		writeCSVFile(node.getPath().getText(), csvRows);
		System.out.println("New row successfully added.");
	}
	
	@Override
	public void outAPutRequest(APutRequest node) {
		ArrayList<Integer> rowIndexes = (ArrayList<Integer>)values.remove(node.getQuery());
		int editedRows;
		
		// Overwrite each selected row.
		if (rowIndexes == null) {
			for (String[] row : csvRows.subList(1, csvRows.size())) {
				assignRow(row);
			}
			editedRows = csvRows.size();
		} else {
			for (int i : rowIndexes) {
				String[] row = csvRows.get(i);
				assignRow(row);
				System.out.println(row[0]);
			}
			editedRows = rowIndexes.size();
		}
		
		writeCSVFile(node.getPath().getText(), csvRows);
		System.out.println(editedRows + " rows successfully edited.");
	}
	
	@Override
	public void outADeleteRequest(ADeleteRequest node) {
		ArrayList<Integer> rowIndexes = (ArrayList<Integer>)values.remove(node.getQuery());
		ListIterator<Integer> itr = rowIndexes.listIterator(rowIndexes.size());
		
		while (itr.hasPrevious()) {
			csvRows.remove((int)itr.previous());
		}
		
		writeCSVFile(node.getPath().getText(), csvRows);
		System.out.println(rowIndexes.size() + " rows successfully deleted.");
	}

	// Assign values to row.
	private void assignRow(String[] row) {
		int nColumns = csvRows.get(0).length;
		// Find selected columns.
		for (int i = 0; i < nColumns; i++) {
			if (selectedColumns.containsKey(i)) {
				Object value = selectedColumns.get(i);
				if (value instanceof ArrayList) {
					value = ((ArrayList) value).get(csvRows.indexOf(row)-1);
				}
				
				// Save number as integer if it has integer value.
				if (value instanceof Double) {
					double number = Double.valueOf(value.toString());
					if (number == Math.floor(number)) {
						row[i] = String.valueOf(((int)Math.floor(number)));
					}
				} else {
					row[i] = selectedColumns.get(i).toString().trim();
				}
			}
		}
	}
	
	// Print table.
	private void printTable(ArrayList<Integer> rowIndexes) {
		int maxlen = 20;
		if (csvRows != null) {
			int i = 1;
			for (String[] row : csvRows.subList(1, csvRows.size())) {
				// Print if current row is on filter or there's no filter.
				if (rowIndexes == null || rowIndexes.contains(i)) {
					// Either print only selected columns or all columns.
					if (selectedColumns != null) {
						for (int j : selectedColumns.keySet()) {
							printCol(row[j], maxlen);
						}
					} else {
						for (String col : row) {
							printCol(col, maxlen);
						}
					}
					System.out.println();
				}
				i++;
			}
		}
	}
	
	// Print a single value.
	private void printCol(String col, int maxlen) {
		// Trim if column is too long.
		int len = col.length();
		if (len >= maxlen) {
			col = col.substring(0, maxlen-4);
			col += "... ";
			len = maxlen;
		}
		
		System.out.print(col);
		
		// Print padding.
		for (int i = 0; i < (maxlen-len); i++)
			System.out.print(' ');
	}
}