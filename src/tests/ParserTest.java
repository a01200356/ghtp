package tests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.PushbackReader;
import java.io.StringReader;

import org.junit.Test;

import ghtp.node.*;
import ghtp.parser.*;
import ghtp.lexer.*;

public class ParserTest {
	
	/**
	 * Get syntax tree as a string.
	 * @param input String to be parsed.
	 * @return String representing syntax tree.
	 * @throws IOException 
	 * @throws LexerException 
	 * @throws ParserException 
 	 */
	private final String getTree(String input) throws ParserException, LexerException, IOException {
		Parser p = new Parser(new Lexer(new PushbackReader(new StringReader(input))));
		Start tree = p.parse();
		TreePrinter printer = new TreePrinter();
		tree.apply(printer);
		return printer.toString();
	}
	
	/*
	 * Auxiliary function to easily see differences between stringified tree.
	 * Used for debugging.
	 */
	private final void printCompare(String a, String b) {
		String A[] = a.split("\n");
		String B[] = b.split("\n");
		for (int i = 0; i < A.length; i++) {
			System.out.println(A[i] + "\n" + B[i]);
		}
		System.out.println("Same lenght: " + (A.length == B.length));
	}
	
	/*
	 * Simplest get request in correct format.
	 */
	@Test
	public final void testSimpleGetRequest() throws ParserException, LexerException, IOException {
		String expectedTree = "Start\n"
				+ " AGetRequest\n";
		
		String actualTree = getTree("GET ./file.csv");
		assertEquals(expectedTree, actualTree);
	}
	
	/*
	 * Simple get request in correct format.
	 */
	@Test
	public final void testGetRequest() throws ParserException, LexerException, IOException {
		String expectedTree = "Start\n"
				+ " AGetRequest\n"
				+ "  AColsSelection\n"
				+ "   ATagColumn\n"
				+ "  ASingleQuery\n"
				+ "   AComparisonCondition\n"
				+ "    AGeEquality\n"
				+ "    ANumberValue\n";
		
		String actualTree = getTree("GET ./alumnos.csv{ nombre } ? calificación >= 8.5");
		assertEquals(expectedTree, actualTree);
	}

	/*
	 * Simple post request in correct format.
	 */
	@Test
	public final void testPostRequest() throws ParserException, LexerException, IOException {
		String expectedTree = "Start\n"
				+ " APostRequest\n"
				+ "  AColsAssignation\n"
				+ "   AAssignation\n"
				+ "    AStringValue\n";
		
		String actualTree = getTree("POST ./alumnos.csv{ nombre = \"Fulanito De Tal\" } ");
		assertEquals(expectedTree, actualTree);
	}
	
	/*
	 * Simple put request in correct format.
	 */
	@Test
	public final void testPutRequest() throws ParserException, LexerException, IOException {
		String expectedTree = "Start\n"
				+ " APutRequest\n"
				+ "  AColsModification\n"
				+ "   AAppendModification\n"
				+ "    AStringValue\n"
				+ "  ASingleQuery\n"
				+ "   AComparisonCondition\n"
				+ "    ALtEquality\n"
				+ "    ANumberValue\n";
		
		String actualTree = getTree("PUT ./alumnos.csv{ nombre += \" Burr@\" } ?calificación < 7");
		assertEquals(expectedTree, actualTree);
	}
	
	/*
	 * Simple delete request in correct format.
	 */
	@Test
	public final void testDeleteRequest() throws ParserException, LexerException, IOException {
		String expectedTree = "Start\n"
				+ " ADeleteRequest\n"
				+ "  ASingleQuery\n"
				+ "   AComparisonCondition\n"
				+ "    ALeEquality\n"
				+ "    ANumberValue\n";
		
		String actualTree = getTree("DELETE ./alumnos.csv?calificación<=5");
		assertEquals(expectedTree, actualTree);
	}
	
	/*
	 * Request with nested operations and functions.
	 */
	@Test
	public final void testNestedStructures() throws ParserException, LexerException, IOException {
		String expectedTree = "Start\n"
				+ " AGetRequest\n"
				+ "  ASingleQuery\n"
				+ "   AComparisonCondition\n"
				+ "    AGeEquality\n"
				+ "    ANumOpValue\n"
				+ "     APlusOperation\n"
				+ "     AParValue\n"
				+ "      ANumOpValue\n"
				+ "       AProdOperation\n"
				+ "       AParValue\n"
				+ "        AParOpValue\n"
				+ "         ANumOpValue\n"
				+ "          ADivOperation\n"
				+ "          ANumberValue\n"
				+ "         AMinusOperation\n"
				+ "         ANumberValue\n";
		
		String actualTree = getTree("GET ./alumnos.csv?calificación>=6+(3*((4/5)-2))");
		assertEquals(expectedTree, actualTree);
	}
	
	/*
	 * Operation involving incompatible types.
	 */
	@Test
	public final void testWrongTypesOperation() throws ParserException, LexerException, IOException {
		String expectedTree = "Start\n"
				+ " AGetRequest\n"
				+ "  ASingleQuery\n"
				+ "   AComparisonCondition\n"
				+ "    AEqualsEquality\n"
				+ "    AParValue\n"
				+ "     AStrOpValue\n"
				+ "      AMinusOperation\n"
				+ "      ANumberValue\n";
		
		String actualTree = getTree("GET ./file? name = (\"text\" - 2)");
		assertEquals(expectedTree, actualTree);
	}
	
	/*
	 * Request has no method (get, post, put, delete).
	 */
	@Test(expected = ParserException.class)
	public final void testNoMethod() throws LexerException, IOException, ParserException {
		getTree("./file.csv");
	}
	
	/*
	 * Request has no path.
	 */
	@Test(expected = ParserException.class)
	public final void testNoPath() throws LexerException, IOException, ParserException {
		getTree("POST (2+2)");
	}
	
	/*
	 * Post request with no column being set.
	 */
	@Test(expected = ParserException.class)
	public final void testPostWithoutCols() throws LexerException, IOException, ParserException {
		getTree("POST ./file.csv");
	}
	
	/*
	 * Put request with no column being set.
	 */
	@Test(expected = ParserException.class)
	public final void testPutWithoutCols() throws LexerException, IOException, ParserException {
		getTree("PUT ./file.csv?title=\"The Title\" ");
	}
	
	/*
	 * Post requests should not have a query.
	 */
	@Test(expected = ParserException.class)
	public final void testPostWithQuery() throws LexerException, IOException, ParserException {
		getTree("POST ./file.csv{column=3}? a > 3");
	}
}
