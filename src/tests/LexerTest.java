package tests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.PushbackReader;
import java.io.StringReader;
import java.util.ArrayList;

import org.junit.Test;

import com.sun.org.apache.xalan.internal.utils.Objects;

import ghtp.lexer.*;
import ghtp.node.*;

public class LexerTest {

	/**
	 * Auxiliary method to run lexer.
	 * @param input String to be parsed.
	 * @return ArrayList with obtained tokens.
	 * @throws IOException 
	 * @throws LexerException 
	 */
	private final Token[] runLexer(String input) throws LexerException, IOException  {
		Lexer lex = new Lexer (new PushbackReader(new StringReader(input)));
		ArrayList<Token> tokens = new ArrayList<>();
		do {
			tokens.add(lex.next());
			//System.out.println(tokens.get(tokens.size()-1).getText());
		} while (tokens.get(tokens.size()-1) instanceof ghtp.node.EOF == false);
		
		return tokens.toArray(new Token[tokens.size()]);
	}
	
	/**
	 * Auxiliary method to compare token arrays.
	 * Only takes token class and text content into consideration.
	 * @param a A token array to compare.
	 * @param b Another token array to compare.
	 * @return True if they are equivalent, false otherwise.
	 */
	private final boolean compareTokens(Token[] a, Token[] b) {
		if (a.length != b.length) {
			System.out.println(a.length + " != " + b.length);
			return false;
		}
		
		for (int i = 0; i < a.length; i++) {
			if (a[i].getClass() != b[i].getClass() || !Objects.equals(a[i].getText(), b[i].getText())) {
				System.out.println("<" + a[i].getText() + "> != <" + b[i].getText() + ">");
				return false;
			}
		}
		
		return true;
	}
	
	/*
	 * Simple request in correct format.
	 */
	@Test
	public final void testSimpleRequest() throws LexerException, IOException {
		Token[] expectedTokens = {
			new TGet("GET "), new TPath("./alumnos.csv"), new TOpBr(), new TTag("nombre"), new TClBr(),
			new TQmark(), new TTag("calificaci%C3%B3n"), new TGe(), new TNumber("8.5"), new EOF()
		};
		Token[] actualTokens = runLexer("GET ./alumnos.csv{nombre}?calificaci%C3%B3n>=8.5");
		
		assertTrue(compareTokens(expectedTokens, actualTokens));
	}

	/*
	 * Some of the strings contain invisible LTR marks (\u200e) for correct displaying.
	 * Without the marks it still parses correctly, but display gets wonky.
	 */
	@Test
	public final void testBidiRequest() throws LexerException, IOException {
		Token[] expectedTokens = {
			new TGet("GET "), new TPath("./بلد.csv"), new TOpBr(), new TTag("العاصمة‎"), new TClBr(),
			new TQmark(), new TTag("لغة‎"), new TEquals(), new TString("\"العربية\""), new EOF()
		};
		Token[] actualTokens = runLexer("GET ./بلد.csv{العاصمة‎}?لغة‎=\"العربية\"");
		
		assertTrue(compareTokens(expectedTokens, actualTokens));
	}
	
	/*
	 * Functions can be nested.
	 * Operations can also have nested parenthesis to force a priority.
	 */
	@Test
	public final void testNestedStructures() throws LexerException, IOException {
		Token[] expectedTokens = {
			new TTag("value"), new TSpace(" "), new TLe(),
			new TSpace(" "), new TOpPar(), new TNumber("2"), new TProd(), new TOpPar(),
			new TNumber("3.5"), new TPlus(), new TNumber("6"), new TDiv(),
			new TNumber("14"), new TClPar(), new TClPar(), new EOF()
		};
		Token[] actualTokens = runLexer("value <= (2*(3.5+6/14))");
		
		assertTrue(compareTokens(expectedTokens, actualTokens));
	}
	
	/*
	 * Values may be defined with the name function and be used as tags.
	 */
	@Test
	public final void testValueDefinition() throws LexerException, IOException {
		Token[] expectedTokens = {
			new TGet("GET "), new TPath("./file.csv"), new TOpBr(), new TTag("NewVal"), new TComma(","),
			new TSpace(" "), new TOpPar(), new TClPar(), new TClBr(), new TQmark(),
			new TOpPar(), new TTag("NewVal"), new TComma(","), new TSpace(" "),
			new TTag("col1"), new TProd(), new TTag("col2"), new TClPar(), new TAnd(), new TTag("NewVal"),
			new TGt(), new TNumber("100"), new EOF()
		};
		Token[] actualTokens = runLexer("GET ./file.csv{NewVal, ()}?(NewVal, col1*col2)&NewVal>100");
		
		assertTrue(compareTokens(expectedTokens, actualTokens));
	}
	
	/*
	 * Digits and special ascii characters are allowed on tags and paths, but need to be escaped.
	 * Quotation marks on strings must also be escaped.
	 */
	@Test
	public final void testTypes() throws LexerException, IOException {
		Token[] expectedTokens = {
			new TTag("Column\\ 1"), new TString("\"This is a\\\" string.\""), new TMinus(),
			new TNumber("1.3E-6"), new TPath("/home/dir.d/file\\_\\2"), new EOF()
		};
		Token[] actualTokens = runLexer("Column\\ 1\"This is a\\\" string.\"-1.3E-6/home/dir.d/file\\_\\2");
		
		assertTrue(compareTokens(expectedTokens, actualTokens));
	}
	
	/*
	 * All token classes except for types, which are tested on testTypes.
	 */
	@Test
	public final void testEverything() throws LexerException, IOException {
		Token[] expectedTokens = {
			new TGet("get "), new TPost("post "), new TPut("put "), new TDelete("delete "),
			new TQmark(), new TOpPar(), new TClPar(), new TComma(";"), new TOpBr(), new TClBr(),
			new TAnd(), new TOr(), new TEquals(), new TLike(), new TUnequal(), new TUnlike("!~="), new TGt(),
			new TLt(), new TGe(), new TLe(), new TPlus(), new TMinus(), new TProd(), new TDiv(), new TAppend(), new EOF()
		};
		Token[] actualTokens = runLexer("get post put delete ?();{}&|=~=!=!~=><>=<=+-*/+=");
		
		assertTrue(compareTokens(expectedTokens, actualTokens));
	}
	
	/*
	 * Any non escaped special ascii characters that don't mean anything.
	 */
	@Test(expected = LexerException.class)
	public final void testIncorrectToken() throws LexerException, IOException {
		runLexer("POST `$#!`");
	}
}