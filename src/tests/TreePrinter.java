package tests;
import ghtp.analysis.*;
import ghtp.node.*;

/*
 * Auxiliary class for printing syntax trees.
 */
public class TreePrinter extends DepthFirstAdapter {

	private int indent = 0;
	private String text = "";

	private void nodeToText(Node node) {
		for (int i = 0; i < this.indent; i++) {
			text += " ";
		}
		text += node.getClass().getSimpleName() + "\n";
	}
	
	@Override
	public void defaultIn(Node node) {
		nodeToText(node);
		this.indent++;
	}

	@Override
	public void defaultOut(Node node) {
		this.indent--;
	}

	@Override
	public String toString() {
		return this.text;
	}
}
