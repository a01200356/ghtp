package tests;

import static org.junit.Assert.*;

import java.io.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import evaluator.Evaluator;
import ghtp.lexer.Lexer;
import ghtp.lexer.LexerException;
import ghtp.node.*;
import ghtp.parser.Parser;
import ghtp.parser.ParserException;

public class EvalTest {

	/**
	 * Auxiliary method to evaluate query.
	 * @param input String to be executed.
	 * @return String with what the query printed.
	 * @throws ParserException 
	 * @throws IOException 
	 * @throws LexerException 
	 */
	private String evalQuery(String query) throws ParserException, LexerException, IOException {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		System.setOut(new PrintStream(stream));
		
		Lexer lex = new Lexer(new PushbackReader(new StringReader(query)));
		
		Parser p = new Parser(lex);
		Start tree = p.parse();
		Evaluator.eval(tree);
		
		System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
		
		return stream.toString();
	}
	
	@Before
	public final void setUp() throws ParserException, LexerException, IOException {
		evalQuery("POST ./src/tests/csvexamples/students.csv{id=101, grade=90}");
	}
	
	@After
	public final void cleanUp() throws ParserException, LexerException, IOException {
		evalQuery("DELETE ./src/tests/csvexamples/students.csv?id=101");
	}
	
	/*
	 * Simplest get request in correct format.
	 */
	@Test
	public final void testSimpleGetRequest() throws ParserException, LexerException, IOException {
		//System.out.println(new File("").getAbsoluteFile());
		String query = "GET ./src/tests/csvexamples/students.csv";
		String response = evalQuery(query);
		assertEquals(response.split("\n").length, 101);
	}
	
	/*
	 * Get request with numeric comparison.
	 */
	@Test
	public final void testGetNumericCompareRequest() throws ParserException, LexerException, IOException {
		String query = "GET ./src/tests/csvexamples/students.csv?id<=10";
		String response = evalQuery(query);
		assertEquals(response.split("\n").length, 10);
	}
	
	/*
	 * Get request with string comparison.
	 */
	@Test
	public final void testGetStringCompareRequest() throws ParserException, LexerException, IOException {
		String query = "GET ./src/tests/csvexamples/students.csv?name~=\"Andrew\"";
		String response = evalQuery(query);
		assertEquals(response.split("\n").length, 3);
	}
	
	/*
	 * Get request with column comparison.
	 */
	@Test
	public final void testGetColumnCompareRequest() throws ParserException, LexerException, IOException {
		String query = "GET ./src/tests/csvexamples/students.csv?grade!=1+grade-1";
		String response = evalQuery(query);
		assertEquals(response.length(), 0);
	}
	
	/*
	 * Get request with nested operations.
	 */
	@Test
	public final void testGetNestedRequest() throws ParserException, LexerException, IOException {
		String query = "GET ./src/tests/csvexamples/students.csv?id!=id | (id>50 & id <= (id/((id/2)+(1/2)*id))+49)";
		String response = evalQuery(query);
		assertEquals(response.length(), 0);
	}
	
	/*
	 * Get request with RTL Unicode strings.
	 */
	@Test
	public final void testGetRTLUnicodeRequest() throws ParserException, LexerException, IOException {
		String query = "GET ./src/tests/csvexamples/کشور.csv?جمعیت>1000000";
		String response = evalQuery(query);
		assertEquals(response.split("\n").length, 157);
	}
	
	/*
	 * Simple post request.
	 */
	@Test
	public final void testPostRequest() throws ParserException, LexerException, IOException {
		String query = "POST ./src/tests/csvexamples/students.csv{id=101, name=\"El Nuevo\", debt=\"$0.00\"}";
		evalQuery(query);
		
		// Assert that there's exactly one more row in the file than before.
		query = "GET ./src/tests/csvexamples/students.csv";
		String response = evalQuery(query);
		assertEquals(response.split("\n").length, 102);
		
		// Assert that there's one row with all the input parameters.
		query = "GET ./src/tests/csvexamples/students.csv?id=101&name=\"El Nuevo\"&debt=0";
		response = evalQuery(query);
		assertEquals(response.split("\n").length, 1);
		assertTrue(response.length() > 0);
	}
	
	/*
	 * Simple put request.
	 */
	@Test
	public final void testPutRequest() throws ParserException, LexerException, IOException {
		String query = "PUT ./src/tests/csvexamples/students.csv{grade+=10}?id=101";
		evalQuery(query);
		
		// Assert that grade was updated on that row.
		query = "GET ./src/tests/csvexamples/students.csv{grade}?id=101";
		String response = evalQuery(query);
		assertEquals(Double.valueOf(response.trim()), Double.valueOf(100));
	}
	
	/*
	 * Simple delete request.
	 */
	@Test
	public final void testDeleteRequest() throws ParserException, LexerException, IOException {
		String query = "DELETE ./src/tests/csvexamples/students.csv?id=101";
		evalQuery(query);
		
		// Assert that there's exactly one less row in the file than before.
		query = "GET ./src/tests/csvexamples/students.csv";
		String response = evalQuery(query);
		assertEquals(response.split("\n").length, 100);
		
		// Assert that deleted row is not there anymore.
		query = "GET ./src/tests/csvexamples/students.csv?id=101";
		response = evalQuery(query);
		assertEquals(response.length(), 0);
	}
	
	/*
	 * Compare name (a string column) with a number.
	 */
	@Test(expected = NumberFormatException.class)
	public final void testWrongTypeComparison() throws ParserException, LexerException, IOException {
		String query = "GET ./src/tests/csvexamples/students.csv?name = 1337";
		evalQuery(query);
	}
	
	/*
	 * Call non-existent file.
	 */
	@Test(expected = RuntimeException.class)
	public final void testNonExistentFile() throws ParserException, LexerException, IOException {
		String query = "GET ./src/tests/csvexamples/TheSecretOfEternalLife.csv?";
		evalQuery(query);
	}
	
	/*
	 * Call non-existent column.
	 */
	@Test(expected = RuntimeException.class)
	public final void testNonExistentColumn() throws ParserException, LexerException, IOException {
		String query = "GET ./src/tests/csvexamples/students.csv?favouriteFood = \"onion\"";
		evalQuery(query);
	}
}
