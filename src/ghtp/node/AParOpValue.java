/* This file was generated by SableCC (http://www.sablecc.org/). */

package ghtp.node;

import ghtp.analysis.*;

@SuppressWarnings("nls")
public final class AParOpValue extends PValue
{
    private TOpPar _opPar_;
    private PValue _insideValue_;
    private TClPar _clPar_;
    private POperation _operation_;
    private PValue _outsideValue_;

    public AParOpValue()
    {
        // Constructor
    }

    public AParOpValue(
        @SuppressWarnings("hiding") TOpPar _opPar_,
        @SuppressWarnings("hiding") PValue _insideValue_,
        @SuppressWarnings("hiding") TClPar _clPar_,
        @SuppressWarnings("hiding") POperation _operation_,
        @SuppressWarnings("hiding") PValue _outsideValue_)
    {
        // Constructor
        setOpPar(_opPar_);

        setInsideValue(_insideValue_);

        setClPar(_clPar_);

        setOperation(_operation_);

        setOutsideValue(_outsideValue_);

    }

    @Override
    public Object clone()
    {
        return new AParOpValue(
            cloneNode(this._opPar_),
            cloneNode(this._insideValue_),
            cloneNode(this._clPar_),
            cloneNode(this._operation_),
            cloneNode(this._outsideValue_));
    }

    public void apply(Switch sw)
    {
        ((Analysis) sw).caseAParOpValue(this);
    }

    public TOpPar getOpPar()
    {
        return this._opPar_;
    }

    public void setOpPar(TOpPar node)
    {
        if(this._opPar_ != null)
        {
            this._opPar_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._opPar_ = node;
    }

    public PValue getInsideValue()
    {
        return this._insideValue_;
    }

    public void setInsideValue(PValue node)
    {
        if(this._insideValue_ != null)
        {
            this._insideValue_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._insideValue_ = node;
    }

    public TClPar getClPar()
    {
        return this._clPar_;
    }

    public void setClPar(TClPar node)
    {
        if(this._clPar_ != null)
        {
            this._clPar_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._clPar_ = node;
    }

    public POperation getOperation()
    {
        return this._operation_;
    }

    public void setOperation(POperation node)
    {
        if(this._operation_ != null)
        {
            this._operation_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._operation_ = node;
    }

    public PValue getOutsideValue()
    {
        return this._outsideValue_;
    }

    public void setOutsideValue(PValue node)
    {
        if(this._outsideValue_ != null)
        {
            this._outsideValue_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._outsideValue_ = node;
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._opPar_)
            + toString(this._insideValue_)
            + toString(this._clPar_)
            + toString(this._operation_)
            + toString(this._outsideValue_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._opPar_ == child)
        {
            this._opPar_ = null;
            return;
        }

        if(this._insideValue_ == child)
        {
            this._insideValue_ = null;
            return;
        }

        if(this._clPar_ == child)
        {
            this._clPar_ = null;
            return;
        }

        if(this._operation_ == child)
        {
            this._operation_ = null;
            return;
        }

        if(this._outsideValue_ == child)
        {
            this._outsideValue_ = null;
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._opPar_ == oldChild)
        {
            setOpPar((TOpPar) newChild);
            return;
        }

        if(this._insideValue_ == oldChild)
        {
            setInsideValue((PValue) newChild);
            return;
        }

        if(this._clPar_ == oldChild)
        {
            setClPar((TClPar) newChild);
            return;
        }

        if(this._operation_ == oldChild)
        {
            setOperation((POperation) newChild);
            return;
        }

        if(this._outsideValue_ == oldChild)
        {
            setOutsideValue((PValue) newChild);
            return;
        }

        throw new RuntimeException("Not a child.");
    }
}
