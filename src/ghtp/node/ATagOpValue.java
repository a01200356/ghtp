/* This file was generated by SableCC (http://www.sablecc.org/). */

package ghtp.node;

import ghtp.analysis.*;

@SuppressWarnings("nls")
public final class ATagOpValue extends PValue
{
    private TTag _tag_;
    private POperation _operation_;
    private PValue _value_;

    public ATagOpValue()
    {
        // Constructor
    }

    public ATagOpValue(
        @SuppressWarnings("hiding") TTag _tag_,
        @SuppressWarnings("hiding") POperation _operation_,
        @SuppressWarnings("hiding") PValue _value_)
    {
        // Constructor
        setTag(_tag_);

        setOperation(_operation_);

        setValue(_value_);

    }

    @Override
    public Object clone()
    {
        return new ATagOpValue(
            cloneNode(this._tag_),
            cloneNode(this._operation_),
            cloneNode(this._value_));
    }

    public void apply(Switch sw)
    {
        ((Analysis) sw).caseATagOpValue(this);
    }

    public TTag getTag()
    {
        return this._tag_;
    }

    public void setTag(TTag node)
    {
        if(this._tag_ != null)
        {
            this._tag_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._tag_ = node;
    }

    public POperation getOperation()
    {
        return this._operation_;
    }

    public void setOperation(POperation node)
    {
        if(this._operation_ != null)
        {
            this._operation_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._operation_ = node;
    }

    public PValue getValue()
    {
        return this._value_;
    }

    public void setValue(PValue node)
    {
        if(this._value_ != null)
        {
            this._value_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._value_ = node;
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._tag_)
            + toString(this._operation_)
            + toString(this._value_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._tag_ == child)
        {
            this._tag_ = null;
            return;
        }

        if(this._operation_ == child)
        {
            this._operation_ = null;
            return;
        }

        if(this._value_ == child)
        {
            this._value_ = null;
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._tag_ == oldChild)
        {
            setTag((TTag) newChild);
            return;
        }

        if(this._operation_ == oldChild)
        {
            setOperation((POperation) newChild);
            return;
        }

        if(this._value_ == oldChild)
        {
            setValue((PValue) newChild);
            return;
        }

        throw new RuntimeException("Not a child.");
    }
}
