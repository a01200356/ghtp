/* This file was generated by SableCC (http://www.sablecc.org/). */

package ghtp.node;

import ghtp.analysis.*;

@SuppressWarnings("nls")
public final class TOpBr extends Token
{
    public TOpBr()
    {
        super.setText("{");
    }

    public TOpBr(int line, int pos)
    {
        super.setText("{");
        setLine(line);
        setPos(pos);
    }

    @Override
    public Object clone()
    {
      return new TOpBr(getLine(), getPos());
    }

    public void apply(Switch sw)
    {
        ((Analysis) sw).caseTOpBr(this);
    }

    @Override
    public void setText(@SuppressWarnings("unused") String text)
    {
        throw new RuntimeException("Cannot change TOpBr text.");
    }
}
