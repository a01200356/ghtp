/* This file was generated by SableCC (http://www.sablecc.org/). */

package ghtp.node;

import ghtp.analysis.*;

@SuppressWarnings("nls")
public final class AAssignation extends PAssignation
{
    private TTag _tag_;
    private TEquals _equals_;
    private PValue _value_;

    public AAssignation()
    {
        // Constructor
    }

    public AAssignation(
        @SuppressWarnings("hiding") TTag _tag_,
        @SuppressWarnings("hiding") TEquals _equals_,
        @SuppressWarnings("hiding") PValue _value_)
    {
        // Constructor
        setTag(_tag_);

        setEquals(_equals_);

        setValue(_value_);

    }

    @Override
    public Object clone()
    {
        return new AAssignation(
            cloneNode(this._tag_),
            cloneNode(this._equals_),
            cloneNode(this._value_));
    }

    public void apply(Switch sw)
    {
        ((Analysis) sw).caseAAssignation(this);
    }

    public TTag getTag()
    {
        return this._tag_;
    }

    public void setTag(TTag node)
    {
        if(this._tag_ != null)
        {
            this._tag_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._tag_ = node;
    }

    public TEquals getEquals()
    {
        return this._equals_;
    }

    public void setEquals(TEquals node)
    {
        if(this._equals_ != null)
        {
            this._equals_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._equals_ = node;
    }

    public PValue getValue()
    {
        return this._value_;
    }

    public void setValue(PValue node)
    {
        if(this._value_ != null)
        {
            this._value_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._value_ = node;
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._tag_)
            + toString(this._equals_)
            + toString(this._value_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._tag_ == child)
        {
            this._tag_ = null;
            return;
        }

        if(this._equals_ == child)
        {
            this._equals_ = null;
            return;
        }

        if(this._value_ == child)
        {
            this._value_ = null;
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._tag_ == oldChild)
        {
            setTag((TTag) newChild);
            return;
        }

        if(this._equals_ == oldChild)
        {
            setEquals((TEquals) newChild);
            return;
        }

        if(this._value_ == oldChild)
        {
            setValue((PValue) newChild);
            return;
        }

        throw new RuntimeException("Not a child.");
    }
}
