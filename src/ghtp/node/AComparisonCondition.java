/* This file was generated by SableCC (http://www.sablecc.org/). */

package ghtp.node;

import ghtp.analysis.*;

@SuppressWarnings("nls")
public final class AComparisonCondition extends PCondition
{
    private TTag _tag_;
    private PEquality _equality_;
    private PValue _value_;

    public AComparisonCondition()
    {
        // Constructor
    }

    public AComparisonCondition(
        @SuppressWarnings("hiding") TTag _tag_,
        @SuppressWarnings("hiding") PEquality _equality_,
        @SuppressWarnings("hiding") PValue _value_)
    {
        // Constructor
        setTag(_tag_);

        setEquality(_equality_);

        setValue(_value_);

    }

    @Override
    public Object clone()
    {
        return new AComparisonCondition(
            cloneNode(this._tag_),
            cloneNode(this._equality_),
            cloneNode(this._value_));
    }

    public void apply(Switch sw)
    {
        ((Analysis) sw).caseAComparisonCondition(this);
    }

    public TTag getTag()
    {
        return this._tag_;
    }

    public void setTag(TTag node)
    {
        if(this._tag_ != null)
        {
            this._tag_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._tag_ = node;
    }

    public PEquality getEquality()
    {
        return this._equality_;
    }

    public void setEquality(PEquality node)
    {
        if(this._equality_ != null)
        {
            this._equality_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._equality_ = node;
    }

    public PValue getValue()
    {
        return this._value_;
    }

    public void setValue(PValue node)
    {
        if(this._value_ != null)
        {
            this._value_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._value_ = node;
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._tag_)
            + toString(this._equality_)
            + toString(this._value_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._tag_ == child)
        {
            this._tag_ = null;
            return;
        }

        if(this._equality_ == child)
        {
            this._equality_ = null;
            return;
        }

        if(this._value_ == child)
        {
            this._value_ = null;
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._tag_ == oldChild)
        {
            setTag((TTag) newChild);
            return;
        }

        if(this._equality_ == oldChild)
        {
            setEquality((PEquality) newChild);
            return;
        }

        if(this._value_ == oldChild)
        {
            setValue((PValue) newChild);
            return;
        }

        throw new RuntimeException("Not a child.");
    }
}
