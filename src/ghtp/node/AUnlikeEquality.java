/* This file was generated by SableCC (http://www.sablecc.org/). */

package ghtp.node;

import ghtp.analysis.*;

@SuppressWarnings("nls")
public final class AUnlikeEquality extends PEquality
{
    private TUnlike _unlike_;

    public AUnlikeEquality()
    {
        // Constructor
    }

    public AUnlikeEquality(
        @SuppressWarnings("hiding") TUnlike _unlike_)
    {
        // Constructor
        setUnlike(_unlike_);

    }

    @Override
    public Object clone()
    {
        return new AUnlikeEquality(
            cloneNode(this._unlike_));
    }

    public void apply(Switch sw)
    {
        ((Analysis) sw).caseAUnlikeEquality(this);
    }

    public TUnlike getUnlike()
    {
        return this._unlike_;
    }

    public void setUnlike(TUnlike node)
    {
        if(this._unlike_ != null)
        {
            this._unlike_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._unlike_ = node;
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._unlike_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._unlike_ == child)
        {
            this._unlike_ = null;
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._unlike_ == oldChild)
        {
            setUnlike((TUnlike) newChild);
            return;
        }

        throw new RuntimeException("Not a child.");
    }
}
