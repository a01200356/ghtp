/* This file was generated by SableCC (http://www.sablecc.org/). */

package ghtp.analysis;

import java.util.*;
import ghtp.node.*;

public class DepthFirstAdapter extends AnalysisAdapter
{
    public void inStart(Start node)
    {
        defaultIn(node);
    }

    public void outStart(Start node)
    {
        defaultOut(node);
    }

    public void defaultIn(@SuppressWarnings("unused") Node node)
    {
        // Do nothing
    }

    public void defaultOut(@SuppressWarnings("unused") Node node)
    {
        // Do nothing
    }

    @Override
    public void caseStart(Start node)
    {
        inStart(node);
        node.getPRequest().apply(this);
        node.getEOF().apply(this);
        outStart(node);
    }

    public void inAGetRequest(AGetRequest node)
    {
        defaultIn(node);
    }

    public void outAGetRequest(AGetRequest node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAGetRequest(AGetRequest node)
    {
        inAGetRequest(node);
        if(node.getGet() != null)
        {
            node.getGet().apply(this);
        }
        if(node.getPath() != null)
        {
            node.getPath().apply(this);
        }
        if(node.getColsSelection() != null)
        {
            node.getColsSelection().apply(this);
        }
        if(node.getQmark() != null)
        {
            node.getQmark().apply(this);
        }
        if(node.getQuery() != null)
        {
            node.getQuery().apply(this);
        }
        outAGetRequest(node);
    }

    public void inADeleteRequest(ADeleteRequest node)
    {
        defaultIn(node);
    }

    public void outADeleteRequest(ADeleteRequest node)
    {
        defaultOut(node);
    }

    @Override
    public void caseADeleteRequest(ADeleteRequest node)
    {
        inADeleteRequest(node);
        if(node.getDelete() != null)
        {
            node.getDelete().apply(this);
        }
        if(node.getPath() != null)
        {
            node.getPath().apply(this);
        }
        if(node.getColsSelection() != null)
        {
            node.getColsSelection().apply(this);
        }
        if(node.getQmark() != null)
        {
            node.getQmark().apply(this);
        }
        if(node.getQuery() != null)
        {
            node.getQuery().apply(this);
        }
        outADeleteRequest(node);
    }

    public void inAPostRequest(APostRequest node)
    {
        defaultIn(node);
    }

    public void outAPostRequest(APostRequest node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAPostRequest(APostRequest node)
    {
        inAPostRequest(node);
        if(node.getPost() != null)
        {
            node.getPost().apply(this);
        }
        if(node.getPath() != null)
        {
            node.getPath().apply(this);
        }
        if(node.getColsAssignation() != null)
        {
            node.getColsAssignation().apply(this);
        }
        if(node.getQmark() != null)
        {
            node.getQmark().apply(this);
        }
        outAPostRequest(node);
    }

    public void inAPutRequest(APutRequest node)
    {
        defaultIn(node);
    }

    public void outAPutRequest(APutRequest node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAPutRequest(APutRequest node)
    {
        inAPutRequest(node);
        if(node.getPut() != null)
        {
            node.getPut().apply(this);
        }
        if(node.getPath() != null)
        {
            node.getPath().apply(this);
        }
        if(node.getColsModification() != null)
        {
            node.getColsModification().apply(this);
        }
        if(node.getQmark() != null)
        {
            node.getQmark().apply(this);
        }
        if(node.getQuery() != null)
        {
            node.getQuery().apply(this);
        }
        outAPutRequest(node);
    }

    public void inAColsAssignation(AColsAssignation node)
    {
        defaultIn(node);
    }

    public void outAColsAssignation(AColsAssignation node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAColsAssignation(AColsAssignation node)
    {
        inAColsAssignation(node);
        if(node.getOpBr() != null)
        {
            node.getOpBr().apply(this);
        }
        if(node.getAssignation() != null)
        {
            node.getAssignation().apply(this);
        }
        {
            List<PExtraAssignation> copy = new ArrayList<PExtraAssignation>(node.getExtraAssignation());
            for(PExtraAssignation e : copy)
            {
                e.apply(this);
            }
        }
        if(node.getClBr() != null)
        {
            node.getClBr().apply(this);
        }
        outAColsAssignation(node);
    }

    public void inAAssignation(AAssignation node)
    {
        defaultIn(node);
    }

    public void outAAssignation(AAssignation node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAAssignation(AAssignation node)
    {
        inAAssignation(node);
        if(node.getTag() != null)
        {
            node.getTag().apply(this);
        }
        if(node.getEquals() != null)
        {
            node.getEquals().apply(this);
        }
        if(node.getValue() != null)
        {
            node.getValue().apply(this);
        }
        outAAssignation(node);
    }

    public void inAExtraAssignation(AExtraAssignation node)
    {
        defaultIn(node);
    }

    public void outAExtraAssignation(AExtraAssignation node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAExtraAssignation(AExtraAssignation node)
    {
        inAExtraAssignation(node);
        if(node.getComma() != null)
        {
            node.getComma().apply(this);
        }
        if(node.getAssignation() != null)
        {
            node.getAssignation().apply(this);
        }
        outAExtraAssignation(node);
    }

    public void inAColsModification(AColsModification node)
    {
        defaultIn(node);
    }

    public void outAColsModification(AColsModification node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAColsModification(AColsModification node)
    {
        inAColsModification(node);
        if(node.getOpBr() != null)
        {
            node.getOpBr().apply(this);
        }
        if(node.getModification() != null)
        {
            node.getModification().apply(this);
        }
        {
            List<PExtraModification> copy = new ArrayList<PExtraModification>(node.getExtraModification());
            for(PExtraModification e : copy)
            {
                e.apply(this);
            }
        }
        if(node.getClBr() != null)
        {
            node.getClBr().apply(this);
        }
        outAColsModification(node);
    }

    public void inAAssignModification(AAssignModification node)
    {
        defaultIn(node);
    }

    public void outAAssignModification(AAssignModification node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAAssignModification(AAssignModification node)
    {
        inAAssignModification(node);
        if(node.getTag() != null)
        {
            node.getTag().apply(this);
        }
        if(node.getEquals() != null)
        {
            node.getEquals().apply(this);
        }
        if(node.getValue() != null)
        {
            node.getValue().apply(this);
        }
        outAAssignModification(node);
    }

    public void inAAppendModification(AAppendModification node)
    {
        defaultIn(node);
    }

    public void outAAppendModification(AAppendModification node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAAppendModification(AAppendModification node)
    {
        inAAppendModification(node);
        if(node.getTag() != null)
        {
            node.getTag().apply(this);
        }
        if(node.getAppend() != null)
        {
            node.getAppend().apply(this);
        }
        if(node.getValue() != null)
        {
            node.getValue().apply(this);
        }
        outAAppendModification(node);
    }

    public void inAExtraModification(AExtraModification node)
    {
        defaultIn(node);
    }

    public void outAExtraModification(AExtraModification node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAExtraModification(AExtraModification node)
    {
        inAExtraModification(node);
        if(node.getComma() != null)
        {
            node.getComma().apply(this);
        }
        if(node.getModification() != null)
        {
            node.getModification().apply(this);
        }
        outAExtraModification(node);
    }

    public void inAColsSelection(AColsSelection node)
    {
        defaultIn(node);
    }

    public void outAColsSelection(AColsSelection node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAColsSelection(AColsSelection node)
    {
        inAColsSelection(node);
        if(node.getOpBr() != null)
        {
            node.getOpBr().apply(this);
        }
        if(node.getColumn() != null)
        {
            node.getColumn().apply(this);
        }
        {
            List<PExtraColumn> copy = new ArrayList<PExtraColumn>(node.getExtraColumn());
            for(PExtraColumn e : copy)
            {
                e.apply(this);
            }
        }
        if(node.getClBr() != null)
        {
            node.getClBr().apply(this);
        }
        outAColsSelection(node);
    }

    public void inATagColumn(ATagColumn node)
    {
        defaultIn(node);
    }

    public void outATagColumn(ATagColumn node)
    {
        defaultOut(node);
    }

    @Override
    public void caseATagColumn(ATagColumn node)
    {
        inATagColumn(node);
        if(node.getTag() != null)
        {
            node.getTag().apply(this);
        }
        outATagColumn(node);
    }

    public void inAExtraColumn(AExtraColumn node)
    {
        defaultIn(node);
    }

    public void outAExtraColumn(AExtraColumn node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAExtraColumn(AExtraColumn node)
    {
        inAExtraColumn(node);
        if(node.getComma() != null)
        {
            node.getComma().apply(this);
        }
        if(node.getColumn() != null)
        {
            node.getColumn().apply(this);
        }
        outAExtraColumn(node);
    }

    public void inASingleQuery(ASingleQuery node)
    {
        defaultIn(node);
    }

    public void outASingleQuery(ASingleQuery node)
    {
        defaultOut(node);
    }

    @Override
    public void caseASingleQuery(ASingleQuery node)
    {
        inASingleQuery(node);
        if(node.getCondition() != null)
        {
            node.getCondition().apply(this);
        }
        outASingleQuery(node);
    }

    public void inAMultipleQuery(AMultipleQuery node)
    {
        defaultIn(node);
    }

    public void outAMultipleQuery(AMultipleQuery node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAMultipleQuery(AMultipleQuery node)
    {
        inAMultipleQuery(node);
        if(node.getCondition() != null)
        {
            node.getCondition().apply(this);
        }
        if(node.getLogicalOperation() != null)
        {
            node.getLogicalOperation().apply(this);
        }
        if(node.getQuery() != null)
        {
            node.getQuery().apply(this);
        }
        outAMultipleQuery(node);
    }

    public void inAComparisonCondition(AComparisonCondition node)
    {
        defaultIn(node);
    }

    public void outAComparisonCondition(AComparisonCondition node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAComparisonCondition(AComparisonCondition node)
    {
        inAComparisonCondition(node);
        if(node.getTag() != null)
        {
            node.getTag().apply(this);
        }
        if(node.getEquality() != null)
        {
            node.getEquality().apply(this);
        }
        if(node.getValue() != null)
        {
            node.getValue().apply(this);
        }
        outAComparisonCondition(node);
    }

    public void inAParCondition(AParCondition node)
    {
        defaultIn(node);
    }

    public void outAParCondition(AParCondition node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAParCondition(AParCondition node)
    {
        inAParCondition(node);
        if(node.getOpPar() != null)
        {
            node.getOpPar().apply(this);
        }
        if(node.getQuery() != null)
        {
            node.getQuery().apply(this);
        }
        if(node.getClPar() != null)
        {
            node.getClPar().apply(this);
        }
        outAParCondition(node);
    }

    public void inAExtraParameter(AExtraParameter node)
    {
        defaultIn(node);
    }

    public void outAExtraParameter(AExtraParameter node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAExtraParameter(AExtraParameter node)
    {
        inAExtraParameter(node);
        if(node.getComma() != null)
        {
            node.getComma().apply(this);
        }
        if(node.getValue() != null)
        {
            node.getValue().apply(this);
        }
        outAExtraParameter(node);
    }

    public void inAStringValue(AStringValue node)
    {
        defaultIn(node);
    }

    public void outAStringValue(AStringValue node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAStringValue(AStringValue node)
    {
        inAStringValue(node);
        if(node.getString() != null)
        {
            node.getString().apply(this);
        }
        outAStringValue(node);
    }

    public void inANumberValue(ANumberValue node)
    {
        defaultIn(node);
    }

    public void outANumberValue(ANumberValue node)
    {
        defaultOut(node);
    }

    @Override
    public void caseANumberValue(ANumberValue node)
    {
        inANumberValue(node);
        if(node.getNumber() != null)
        {
            node.getNumber().apply(this);
        }
        outANumberValue(node);
    }

    public void inANegNumValue(ANegNumValue node)
    {
        defaultIn(node);
    }

    public void outANegNumValue(ANegNumValue node)
    {
        defaultOut(node);
    }

    @Override
    public void caseANegNumValue(ANegNumValue node)
    {
        inANegNumValue(node);
        if(node.getMinus() != null)
        {
            node.getMinus().apply(this);
        }
        if(node.getNumber() != null)
        {
            node.getNumber().apply(this);
        }
        outANegNumValue(node);
    }

    public void inATagValue(ATagValue node)
    {
        defaultIn(node);
    }

    public void outATagValue(ATagValue node)
    {
        defaultOut(node);
    }

    @Override
    public void caseATagValue(ATagValue node)
    {
        inATagValue(node);
        if(node.getTag() != null)
        {
            node.getTag().apply(this);
        }
        outATagValue(node);
    }

    public void inAStrOpValue(AStrOpValue node)
    {
        defaultIn(node);
    }

    public void outAStrOpValue(AStrOpValue node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAStrOpValue(AStrOpValue node)
    {
        inAStrOpValue(node);
        if(node.getString() != null)
        {
            node.getString().apply(this);
        }
        if(node.getOperation() != null)
        {
            node.getOperation().apply(this);
        }
        if(node.getValue() != null)
        {
            node.getValue().apply(this);
        }
        outAStrOpValue(node);
    }

    public void inANumOpValue(ANumOpValue node)
    {
        defaultIn(node);
    }

    public void outANumOpValue(ANumOpValue node)
    {
        defaultOut(node);
    }

    @Override
    public void caseANumOpValue(ANumOpValue node)
    {
        inANumOpValue(node);
        if(node.getNumber() != null)
        {
            node.getNumber().apply(this);
        }
        if(node.getOperation() != null)
        {
            node.getOperation().apply(this);
        }
        if(node.getValue() != null)
        {
            node.getValue().apply(this);
        }
        outANumOpValue(node);
    }

    public void inATagOpValue(ATagOpValue node)
    {
        defaultIn(node);
    }

    public void outATagOpValue(ATagOpValue node)
    {
        defaultOut(node);
    }

    @Override
    public void caseATagOpValue(ATagOpValue node)
    {
        inATagOpValue(node);
        if(node.getTag() != null)
        {
            node.getTag().apply(this);
        }
        if(node.getOperation() != null)
        {
            node.getOperation().apply(this);
        }
        if(node.getValue() != null)
        {
            node.getValue().apply(this);
        }
        outATagOpValue(node);
    }

    public void inAParValue(AParValue node)
    {
        defaultIn(node);
    }

    public void outAParValue(AParValue node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAParValue(AParValue node)
    {
        inAParValue(node);
        if(node.getOpPar() != null)
        {
            node.getOpPar().apply(this);
        }
        if(node.getValue() != null)
        {
            node.getValue().apply(this);
        }
        if(node.getClPar() != null)
        {
            node.getClPar().apply(this);
        }
        outAParValue(node);
    }

    public void inAParOpValue(AParOpValue node)
    {
        defaultIn(node);
    }

    public void outAParOpValue(AParOpValue node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAParOpValue(AParOpValue node)
    {
        inAParOpValue(node);
        if(node.getOpPar() != null)
        {
            node.getOpPar().apply(this);
        }
        if(node.getInsideValue() != null)
        {
            node.getInsideValue().apply(this);
        }
        if(node.getClPar() != null)
        {
            node.getClPar().apply(this);
        }
        if(node.getOperation() != null)
        {
            node.getOperation().apply(this);
        }
        if(node.getOutsideValue() != null)
        {
            node.getOutsideValue().apply(this);
        }
        outAParOpValue(node);
    }

    public void inAEqualsOperation(AEqualsOperation node)
    {
        defaultIn(node);
    }

    public void outAEqualsOperation(AEqualsOperation node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAEqualsOperation(AEqualsOperation node)
    {
        inAEqualsOperation(node);
        if(node.getEquals() != null)
        {
            node.getEquals().apply(this);
        }
        outAEqualsOperation(node);
    }

    public void inAPlusOperation(APlusOperation node)
    {
        defaultIn(node);
    }

    public void outAPlusOperation(APlusOperation node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAPlusOperation(APlusOperation node)
    {
        inAPlusOperation(node);
        if(node.getPlus() != null)
        {
            node.getPlus().apply(this);
        }
        outAPlusOperation(node);
    }

    public void inAMinusOperation(AMinusOperation node)
    {
        defaultIn(node);
    }

    public void outAMinusOperation(AMinusOperation node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAMinusOperation(AMinusOperation node)
    {
        inAMinusOperation(node);
        if(node.getMinus() != null)
        {
            node.getMinus().apply(this);
        }
        outAMinusOperation(node);
    }

    public void inAProdOperation(AProdOperation node)
    {
        defaultIn(node);
    }

    public void outAProdOperation(AProdOperation node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAProdOperation(AProdOperation node)
    {
        inAProdOperation(node);
        if(node.getProd() != null)
        {
            node.getProd().apply(this);
        }
        outAProdOperation(node);
    }

    public void inADivOperation(ADivOperation node)
    {
        defaultIn(node);
    }

    public void outADivOperation(ADivOperation node)
    {
        defaultOut(node);
    }

    @Override
    public void caseADivOperation(ADivOperation node)
    {
        inADivOperation(node);
        if(node.getDiv() != null)
        {
            node.getDiv().apply(this);
        }
        outADivOperation(node);
    }

    public void inAAppendOperation(AAppendOperation node)
    {
        defaultIn(node);
    }

    public void outAAppendOperation(AAppendOperation node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAAppendOperation(AAppendOperation node)
    {
        inAAppendOperation(node);
        if(node.getAppend() != null)
        {
            node.getAppend().apply(this);
        }
        outAAppendOperation(node);
    }

    public void inAEqualsEquality(AEqualsEquality node)
    {
        defaultIn(node);
    }

    public void outAEqualsEquality(AEqualsEquality node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAEqualsEquality(AEqualsEquality node)
    {
        inAEqualsEquality(node);
        if(node.getEquals() != null)
        {
            node.getEquals().apply(this);
        }
        outAEqualsEquality(node);
    }

    public void inALikeEquality(ALikeEquality node)
    {
        defaultIn(node);
    }

    public void outALikeEquality(ALikeEquality node)
    {
        defaultOut(node);
    }

    @Override
    public void caseALikeEquality(ALikeEquality node)
    {
        inALikeEquality(node);
        if(node.getLike() != null)
        {
            node.getLike().apply(this);
        }
        outALikeEquality(node);
    }

    public void inAUnequalEquality(AUnequalEquality node)
    {
        defaultIn(node);
    }

    public void outAUnequalEquality(AUnequalEquality node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAUnequalEquality(AUnequalEquality node)
    {
        inAUnequalEquality(node);
        if(node.getUnequal() != null)
        {
            node.getUnequal().apply(this);
        }
        outAUnequalEquality(node);
    }

    public void inAUnlikeEquality(AUnlikeEquality node)
    {
        defaultIn(node);
    }

    public void outAUnlikeEquality(AUnlikeEquality node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAUnlikeEquality(AUnlikeEquality node)
    {
        inAUnlikeEquality(node);
        if(node.getUnlike() != null)
        {
            node.getUnlike().apply(this);
        }
        outAUnlikeEquality(node);
    }

    public void inALtEquality(ALtEquality node)
    {
        defaultIn(node);
    }

    public void outALtEquality(ALtEquality node)
    {
        defaultOut(node);
    }

    @Override
    public void caseALtEquality(ALtEquality node)
    {
        inALtEquality(node);
        if(node.getLt() != null)
        {
            node.getLt().apply(this);
        }
        outALtEquality(node);
    }

    public void inAGtEquality(AGtEquality node)
    {
        defaultIn(node);
    }

    public void outAGtEquality(AGtEquality node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAGtEquality(AGtEquality node)
    {
        inAGtEquality(node);
        if(node.getGt() != null)
        {
            node.getGt().apply(this);
        }
        outAGtEquality(node);
    }

    public void inALeEquality(ALeEquality node)
    {
        defaultIn(node);
    }

    public void outALeEquality(ALeEquality node)
    {
        defaultOut(node);
    }

    @Override
    public void caseALeEquality(ALeEquality node)
    {
        inALeEquality(node);
        if(node.getLe() != null)
        {
            node.getLe().apply(this);
        }
        outALeEquality(node);
    }

    public void inAGeEquality(AGeEquality node)
    {
        defaultIn(node);
    }

    public void outAGeEquality(AGeEquality node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAGeEquality(AGeEquality node)
    {
        inAGeEquality(node);
        if(node.getGe() != null)
        {
            node.getGe().apply(this);
        }
        outAGeEquality(node);
    }

    public void inAAndLogicalOperation(AAndLogicalOperation node)
    {
        defaultIn(node);
    }

    public void outAAndLogicalOperation(AAndLogicalOperation node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAAndLogicalOperation(AAndLogicalOperation node)
    {
        inAAndLogicalOperation(node);
        if(node.getAnd() != null)
        {
            node.getAnd().apply(this);
        }
        outAAndLogicalOperation(node);
    }

    public void inAOrLogicalOperation(AOrLogicalOperation node)
    {
        defaultIn(node);
    }

    public void outAOrLogicalOperation(AOrLogicalOperation node)
    {
        defaultOut(node);
    }

    @Override
    public void caseAOrLogicalOperation(AOrLogicalOperation node)
    {
        inAOrLogicalOperation(node);
        if(node.getOr() != null)
        {
            node.getOr().apply(this);
        }
        outAOrLogicalOperation(node);
    }
}
